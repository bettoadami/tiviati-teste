<?php

use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activities')->insert([
            'name' => str_random(10),
            'description' => str_random(10).' '.str_random(7).' '.str_random(12),
            'status_id' => 1,
            'started_at' => '2018-01-20',
            'done_at' => null,
            'state' => 1
        ]);
        DB::table('activities')->insert([
            'name' => str_random(8),
            'description' => str_random(12).' '.str_random(5).' '.str_random(3),
            'status_id' => 2,
            'started_at' => '2018-01-21',
            'done_at' => null,
            'state' => 0
        ]);
        DB::table('activities')->insert([
            'name' => str_random(10),
            'description' => str_random(10).' '.str_random(7).' '.str_random(12),
            'status_id' => 3,
            'started_at' => '2018-01-22',
            'done_at' => null,
            'state' => 1
        ]);
        DB::table('activities')->insert([
            'name' => str_random(7),
            'description' => str_random(5).' '.str_random(3).' '.str_random(8),
            'status_id' => 4,
            'started_at' => '2018-01-23',
            'done_at' => '2018-01-25',
            'state' => 1
        ]);
    }
}
