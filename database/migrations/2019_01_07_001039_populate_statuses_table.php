<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('statuses')->insert([
            [
                'slug' => 'todo',
                'name' => 'Pendente',
            ],
            [
                'slug' => 'wip',
                'name' => 'Em Desenvolvimento',
            ],
            [
                'slug' => 'qa',
                'name' => 'Em Teste',
            ],
            [
                'slug' => 'done',
                'name' => 'Concluído',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*No actions*/
    }
}
