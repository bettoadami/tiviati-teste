@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <p class="navbar-text navbar-right">
                        <button type="button" onclick="ajaxNew()" class="btn btn-primary">NOVA ATIVIDADE+</button>
                    </p>
                    <h3 class="panel-heading">Lista de Atividades</h3>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>@sortablelink('id', 'ID')</th>
                                <th>@sortablelink('status', 'NOME')</th>
                                <th>@sortablelink('status', 'DESCRIÇÃO')</th>
                                <th>@sortablelink('started_at', 'INÍCIO')</th>
                                <th>@sortablelink('done_at', 'TÉRMINO')</th>
                                <th>@sortablelink('status', 'STATUS')</th>
                                <th>@sortablelink('state', 'SITUAÇÃO')</th>
                                <th>EDIÇÃO</th>
                            </tr>
                            </thead>
                            @if(!empty($activities))
                                @foreach($activities as $activity)
                                    @component('tableline')
                                        @slot('id')
                                            {{$activity->id}}
                                        @endslot
                                        @slot('name')
                                            {{$activity->name}}
                                        @endslot
                                        @slot('description')
                                            {{$activity->description}}
                                        @endslot
                                        @slot('started_at')
                                            {{Date('d/m/Y', strtotime($activity->started_at))}}
                                        @endslot
                                        @slot('done_at')
                                            {{Date('d/m/Y', strtotime($activity->done_at))}}
                                        @endslot
                                        @slot('status')
                                            {{$activity->status->name}}
                                        @endslot
                                        @slot('state')
                                            {{$activity->state ? 'Ativo' : 'Inativo'}}
                                        @endslot
                                    @endcomponent
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!empty($statuses))
        <script>
            var statuslist = {
            @foreach($statuses as $k => $v)
                "{{$k}}":"{{$v}}",
            @endforeach
            }
        </script>
    @endif
@endsection
