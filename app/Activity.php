<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Activity extends Model
{
    use SoftDeletes, Sortable;

    public $timestamps = false;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'description',
        'started_at',
        'done_at',
        'status_id',
        'state'
    ];
    public $sortable = [
        'started_at',
        'done_at',
        'status_id',
        'state'
    ];

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
