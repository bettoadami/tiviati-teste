<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use App\Status;

class ActivityController extends Controller
{
    public function index(Activity $activity, Status $status)
    {
        $statuses = $status->all()->toArray();
        $list = [];
        foreach ($statuses as $s) {
            $id = $s['id'];
            $list[$id] = $s['name'];
        }
        $activities = $activity->with('status')->sortable()->paginate(10);
        return view('home',
            [
                'activities' => $activities,
                'statuses' => $list
            ]
        );
    }

    public function put(Activity $activity, Request $request)
    {
        if ($activity->update($request->toArray())) {
            return response("Sucesso", 201);
        }
        return response("Erro", 400);
    }

    public function post(Activity $activity, Request $request)
    {
        if ($activity->create($request->toArray())) {
            return response("Sucesso", 201);
        }
        return response("Erro", 400);
    }
}
