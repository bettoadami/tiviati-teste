##Instalação

Tendo Virtual Box, Vagrant e GIT instalados, após baixar o repositório, basta:

1 No diretório do projeto executar: vagrant up

2 Acessar a máquina via Git Bash executando: vagrant ssh

3 Executar: bash /vagrant/installation/install.sh

A instalação não demanda interações. Basta aguardar.