#!/bin/bash
PATH=/bin:/usr/bin:

DBPASSWORD=1234
NONE='\033[00m'
RED='\033[01;31m'

# add repos
echo -e "${RED}**** add repos ****${NONE}"
debconf-set-selections <<< 'mysql-apt-config mysql-apt-config/select-server select mysql-5.7'
debconf-set-selections <<< 'mysql-apt-config mysql-apt-config/select-tools select Disabled'
debconf-set-selections <<< 'mysql-apt-config mysql-apt-config/select-preview select Disabled'
debconf-set-selections <<< 'mysql-apt-config mysql-apt-config/select-product select Ok'
wget https://dev.mysql.com/get/mysql-apt-config_0.8.9-1_all.deb
sudo DEBIAN_FRONTEND=noninteractive dpkg -i mysql-apt-config_0.8.9-1_all.deb
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update

# install banner
sudo apt-get install -y sysvbanner

# set up swap space
sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo cp /vagrant/installation/resources/fstab /etc/

# make sure necessary helpers installed
echo -e "${RED}**** install helpers ****${NONE}"
sudo apt-get install -y python-software-properties

# install php
echo -e "${RED}**** install php ****${NONE}"
sudo apt-get install -y php7.3 php7.3-fpm php7.3-gd php7.3-curl php7.3-mysql php7.3-json php7.3-mbstring php7.3-xmlrpc php7.3-soap php7.3-xml php7.3-cli php7.3-zip php7.3-bcmath php7.3-common

# install nginx and git
echo -e "${RED}**** install nginx and git ****${NONE}"
sudo apt-get install -y nginx git

# install mysql
echo -e "${RED}**** install mysql ****${NONE}"
sudo debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password ${DBPASSWORD}"
sudo debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password ${DBPASSWORD}"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server

#install nodejs and npm
echo -e "${RED}**** install nodejs and npm ****${NONE}"
sudo apt-get install -y nodejs
sudo apt-get install -y npm

# install composer
echo -e "${RED}**** install composer ****${NONE}"
sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/bin --filename=composer
sudo php -r "unlink('composer-setup.php');"

# move config files into place
echo -e "${RED}**** install config files ****${NONE}"
sudo cp /vagrant/installation/resources/default /etc/nginx/sites-available/
sudo cp /vagrant/installation/resources/nginx.conf /etc/nginx/
sudo cp /vagrant/installation/resources/www.conf /etc/php/7.3/fpm/pool.d
sudo chown vagrant:vagrant /var/run/php

# restart everything
echo -e "${RED}**** restarting services ****${NONE}"
sudo service nginx restart
sudo service mysql restart
sudo service php7.3-fpm restart

# create database
echo -e "${RED}**** create new database ****${NONE}"
sudo mysql -u root -p$DBPASSWORD < /vagrant/installation/resources/createdatabase.sql

# install laravel
echo -e "${RED}**** install laravel ****${NONE}"
cd /vagrant
sudo chown -R www-data:www-data /vagrant/storage
sudo chown -R www-data:www-data /vagrant/bootstrap/cache
sudo chmod -R 777 /vagrant/storage
sudo chmod -R 777 /vagrant/bootstrap/cache
sudo cp .env.example .env
composer install
php artisan migrate
php artisan db:seed --class=ActivitiesTableSeeder
npm config set registry="http://registry.npmjs.org/"
npm install

# END
clear
echo " "
echo " "
tput setaf 2
banner "C'EST FINI"
tput rev; echo "*****************  Access the site on http://localhost:8080  *****************"; tput sgr0
