function ajaxEdit(numberid){
    swal({
        title: 'Editar Atividade',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Salvar",
        onOpen: function() {
            $( ".datebox" ).datepicker();
        },
        html:
            'Nome: ' +
            '<input id="edit-input1" class="swal2-input" placeholder="Nome">' +
            'Descrição: ' +
            '<input id="edit-input2" class="swal2-input" placeholder="Descrição">' +
            'Início: ' +
            '<input id="edit-input3" class="swal2-input datebox" placeholder="Início">' +
            'Término: ' +
            '<input id="edit-input4" class="swal2-input datebox" placeholder="Término">' +
            'Status: ' +
            '<select id="edit-input5" name="status" class="swal2-input" >' +
            '<option value="1">Pendente</option>' +
            '<option value="2">Em Desenvolvimento</option>' +
            '<option value="3">Em Teste</option>' +
            '<option value="4">Concluído</option>' +
            '</select>' +
            '<br>Situação: <br>' +
            '<input type="radio" name="edit-state" value="1"> Ativo<br>' +
            '<input type="radio" name="edit-state" value="0"> Inativo',
        focusConfirm: false,
        preConfirm: function () {
            return new Promise(function (resolve) {
                resolve([
                    $('#edit-input1').val(),
                    $('#edit-input2').val(),
                    $('#edit-input3').val(),
                    $('#edit-input4').val(),
                    $("select#edit-input5 option:checked").val(),
                    $("input[name='edit-state']:checked").val()
                ])
            })
        }
    }).then(function (params) {
        console.log(JSON.stringify(params))
        $.ajax(
            {
                type: "put",
                url: "/"+numberid,
                data: {
                    name:params[0],
                    description:params[1],
                    status_id:params[2],
                    started_at:params[3],
                    done_at:params[4],
                    state:params[5]
                }
            }
        )
            .done(function (data) {
                swal("Sucesso", "O registro foi alterado.", "success");
                location.reload();
            })
            .error(function (data) {
                swal("Opa!", "A edição falhou. Tente novamente.", "error");
            });
    }).catch(swal.noop)
}

function ajaxNew(){
    swal({
        title: 'Nova Atividade',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Salvar",
        onOpen: function() {
            $( ".datebox" ).datepicker();
        },
        html:
            'Nome: ' +
            '<input id="new-input1" class="swal2-input" placeholder="Nome">' +
            'Descrição: ' +
            '<input id="new-input2" class="swal2-input" placeholder="Descrição">' +
            'Início: ' +
            '<input id="new-input3" class="swal2-input datebox" placeholder="Início">' +
            'Término: ' +
            '<input id="new-input4" class="swal2-input datebox" placeholder="Término">' +
            'Status: ' +
            '<select id="new-input5" name="status" class="swal2-input" >' +
            '<option value="1">Pendente</option>' +
            '<option value="2">Em Desenvolvimento</option>' +
            '<option value="3">Em Teste</option>' +
            '<option value="4">Concluído</option>' +
            '</select>' +
            '<br>Situação: <br>' +
            '<input type="radio" name="new-state" value="1"> Ativo<br>' +
            '<input type="radio" name="new-state" value="0"> Inativo',
        focusConfirm: false,
        preConfirm: function () {
            return new Promise(function (resolve) {
                resolve([
                    $('#new-input1').val(),
                    $('#new-input2').val(),
                    $('#new-input3').val(),
                    $('#new-input4').val(),
                    $("select#new-input5 option:checked").val(),
                    $("input[name='new-state']:checked").val()

                ])
            })
        }
    }).then(function (params) {
        console.log(JSON.stringify(params))
        let date1 = params[3].split("/");
        let date2 = params[4].split("/");
        $.ajax(
            {
                type: "post",
                url: "/",
                data: {
                    name:params[0],
                    description:params[1],
                    status_id:params[2],
                    started_at:date1[2]+'-'+date1[1]+'-'+date1[0],
                    done_at:date2[2]+'-'+date2[1]+'-'+date2[0],
                    state:params[5]
                }
            }
        )
            .done(function (data) {
                swal("Sucesso", "O registro foi criado.", "success");
                //location.reload();
            })
            .error(function (data) {
                swal("Opa!", "A criação falhou. Tente novamente.", "error");
            });
    }).catch(swal.noop)
}
